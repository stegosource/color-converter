import Vue from 'vue'
import App from '@/App'

describe('App.vue', () => {
  it('should have the data hook', () => {
    expect(typeof App.data).to.equal('function')
  })

  it('should have a hexToRgb function', () => {
    const Constructor = Vue.extend(App)
    const vm = new Constructor({
      // Options
    }).$mount();
    expect(typeof vm.hexToRgb).to.equal('function');
  });
});