import Vue from 'vue'
import AppHeader from '@/components/AppHeader'

describe('AppHeader.vue', () => {
  it('should have the correct title', () => {
    const Constructor = Vue.extend(AppHeader)
    const vm = new Constructor({
      // Options
    }).$mount();

    expect(vm.$el.querySelector('h1').textContent).to.equal('Color Value Converter')
  })
});