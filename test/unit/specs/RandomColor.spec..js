// import Vue from 'vue'
// import RandomColor from '@/components/RandomColor'

// describe('RandomColor.vue', () => {
//   it('should have the correct title', () => {
//     const Constructor = Vue.extend(RandomColor)
//     const vm = new Constructor({
//       // Options
//     }).$mount();

//     expect(vm.$el.querySelector('h1').textContent).to.equal('Color Value Converter')
//   })
// });

// // Inspect the generated HTML after a state update
// it('updates the rendered message when vm.message updates', done => {
//   const vm = new Vue(MyComponent).$mount()
//   vm.message = 'foo'
//   // wait a "tick" after state change before asserting DOM updates
//   Vue.nextTick(() => {
//     expect(vm.$el.textContent).toBe('foo')
//     done()
//   })
// })

// // helper function that mounts and returns the rendered text
// function getRenderedText (Component, propsData) {
//   const Ctor = Vue.extend(Component)
//   const vm = new Ctor({ propsData: propsData }).$mount()
//   return vm.$el.textContent
// }
// describe('MyComponent', () => {
//   it('renders correctly with different props', () => {
//     expect(getRenderedText(MyComponent, {
//       msg: 'Hello'
//     })).toBe('Hello')
//     expect(getRenderedText(MyComponent, {
//       msg: 'Bye'
//     })).toBe('Bye')
//   })
// })
